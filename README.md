# SkyDictionary

Sample App client for https://dictionary.skyeng.ru/doc/api/external

## Build

```bash
./gradlew assemble
```
or run `app` with Android Studio


## Libraries

| Url                                            | Purpose                                          |
|------------------------------------------------|--------------------------------------------------|
| https://developer.android.com/jetpack          | For Architecture components, Navigation, Paging  |
| https://github.com/InsertKoinIO/koin           | Kotlin DI                                        |
| https://square.github.io/retrofit/             | HTTP Client                                      |
| https://github.com/square/moshi                | JSON parsing                                     |
| https://github.com/Kotlin/kotlinx.coroutines   | Async work                                       |
| https://github.com/bumptech/glide              | Image loading                                    |
| https://github.com/michaelbull/kotlin-result   | Error handling with sealed class                 |
| https://github.com/mannodermaus/android-junit5 | Android JUnit5 - test runner                     |
| https://github.com/mockk/mockk                 | Mocking library                                  |
| https://github.com/kotest/kotest               | Fluent assertions                                |
| https://github.com/detekt/detekt               | Kotlin static analysis                           |

## License
[MIT](https://choosealicense.com/licenses/mit/)