package com.example.skydictionary.core.util

import com.github.michaelbull.result.unwrap
import com.github.michaelbull.result.unwrapError
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.beInstanceOf
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.lang.IllegalStateException

internal class ResultTest {

    private val testValue = "string"

    @Test
    fun `should map success to Ok`() = runBlockingTest {
        resultOf { testValue }.unwrap() shouldBe testValue
    }

    @TestFactory
    fun `should map errors`() = mapOf(
        HttpException(
            Response.error<String>(
                404,
                "{}".toResponseBody("text/plain".toMediaTypeOrNull())
            )
        ) to Error.Http::class,
        IOException() to Error.Network::class,
        Exception() to Error.Unknown::class,
        IllegalStateException() to Error.Unknown::class,
    ).map { (exception, errorClass) ->
        dynamicTest("should map $exception to ${errorClass.simpleName}") {
            runBlockingTest {
                resultOf { throw exception }.unwrapError() should beInstanceOf(errorClass)
            }
        }
    }
}
