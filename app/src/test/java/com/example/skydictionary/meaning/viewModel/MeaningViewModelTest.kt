package com.example.skydictionary.meaning.viewModel

import androidx.lifecycle.asFlow
import com.example.skydictionary.core.util.Error
import com.example.skydictionary.meaning.usecase.FetchMeanings
import com.example.skydictionary.stub.sampleMeaning
import com.example.skydictionary.stub.sampleWord
import com.example.skydictionary.util.MainThreadExtension
import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import io.kotest.matchers.collections.shouldContainInOrder
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MainThreadExtension::class)
internal class MeaningViewModelTest {

    private val word = sampleWord()
    private val meanings = listOf(sampleMeaning(), sampleMeaning())

    private val fetchMeanings: FetchMeanings = mockk {
        coEvery { ofWord(any()) } returns Ok(meanings)
    }

    private val viewModel = MeaningViewModel(word, fetchMeanings)

    @Test
    fun `should fetch meanings`() = runBlockingTest {
        coVerify { fetchMeanings.ofWord(word) }
        viewModel.meanings.asFlow().first() shouldContainInOrder meanings
    }

    @Test
    fun `should refresh meanings`() = runBlockingTest {
        val refreshed = meanings.reversed()
        coEvery { fetchMeanings.ofWord(any()) } returns Ok(refreshed)
        viewModel.refresh()
        coVerify(exactly = 2) { fetchMeanings.ofWord(word) }
        viewModel.meanings.asFlow().first() shouldContainInOrder refreshed
    }

    @Test
    fun `should show errors`() = runBlockingTest {
        coEvery { fetchMeanings.ofWord(any()) } returns Err(Error.Network("message"))
        viewModel.refresh()
        viewModel.error.asFlow().first().error.message shouldBe "message"
    }
}
