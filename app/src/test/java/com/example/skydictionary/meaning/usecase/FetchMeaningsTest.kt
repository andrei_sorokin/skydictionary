package com.example.skydictionary.meaning.usecase

import com.example.skydictionary.core.util.Error
import com.example.skydictionary.meaning.api.MeaningApi
import com.example.skydictionary.search.entity.MeaningInfo
import com.example.skydictionary.search.entity.Word
import com.example.skydictionary.stub.sampleMeaning
import com.example.skydictionary.stub.sampleWord
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.slot
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Test
import java.io.IOException

internal class FetchMeaningsTest {

    private val meanings = listOf(sampleMeaning(), sampleMeaning())

    private val api: MeaningApi = mockk {
        coEvery { getMeanings(any(), any()) } returns meanings
    }

    private val fetchMeanings = FetchMeanings(api)

    @Test
    fun `should join all ids`() = runBlockingTest {
        val infos = (1..10).map { MeaningInfo(it.toString()) }
        val word = Word("id", "text", infos)
        fetchMeanings.ofWord(word)
        val slot = slot<String>()
        coVerify { api.getMeanings(capture(slot)) }
        slot.captured.split(",") shouldContainExactly infos.map { it.id }
    }

    @Test
    fun `should fetch list`() = runBlockingTest {
        val word = Word("id", "text", (1..10).map { MeaningInfo(it.toString()) })
        val (list, error) = fetchMeanings.ofWord(word)
        list shouldContainExactly meanings
        error shouldBe null
    }

    @Test
    fun `should catch errors`() = runBlockingTest {
        coEvery { api.getMeanings(any(), any()) } throws IOException()
        val (list, error) = fetchMeanings.ofWord(sampleWord())
        list shouldBe null
        error shouldBe Error.Network(null)
    }
}
