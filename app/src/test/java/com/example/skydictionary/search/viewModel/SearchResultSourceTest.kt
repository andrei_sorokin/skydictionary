package com.example.skydictionary.search.viewModel

import androidx.paging.PagingSource
import com.example.skydictionary.core.util.Error
import com.example.skydictionary.search.api.SearchApi
import com.example.skydictionary.stub.sampleWord
import io.kotest.assertions.assertSoftly
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Test
import java.io.IOException

internal class SearchResultSourceTest {

    private val api: SearchApi = mockk()

    private val query = "query"

    private val source = SearchResultSource(api, query)

    private val results = listOf(sampleWord(), sampleWord())

    @Test
    fun `should load new page when received result`() = runBlockingTest {
        coEvery { api.getWords(any(), any(), any()) } returns results

        val result = source.load(params(1))

        assertSoftly {
            val pageResult = (result as PagingSource.LoadResult.Page)
            pageResult.data shouldBe results
            pageResult.nextKey shouldBe 2
        }
    }

    @Test
    fun `should not load new page when empty result`() = runBlockingTest {
        coEvery { api.getWords(any(), any(), any()) } returns emptyList()
        val result = source.load(params(1))

        assertSoftly {
            val pageResult = (result as PagingSource.LoadResult.Page)
            pageResult.data shouldBe emptyList()
            pageResult.nextKey shouldBe null
        }
    }

    @Test
    fun `should wrap error when received`() = runBlockingTest {
        val exception = IOException("some message")
        coEvery { api.getWords(any(), any(), any()) } throws exception
        val result = source.load(params(1))

        val pageResult = (result as PagingSource.LoadResult.Error)
        pageResult.throwable shouldBe Error.Network(exception.message)
    }

    private fun params(page: Int, loadSize: Int = 10) =
        PagingSource.LoadParams.Append(page, loadSize, placeholdersEnabled = true, loadSize)
}
