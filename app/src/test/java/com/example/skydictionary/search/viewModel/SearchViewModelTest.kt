package com.example.skydictionary.search.viewModel

import androidx.lifecycle.asFlow
import androidx.paging.PagingSource
import com.example.skydictionary.util.MainThreadExtension
import com.example.skydictionary.util.testDispatcher
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MainThreadExtension::class)
internal class SearchViewModelTest {

    private val query = "query"

    private val searchSourceFactory: SearchSourceFactory = mockk {
        every { createSource(any()) } returns mockk(relaxed = true) {
            coEvery { load(any()) } returns PagingSource.LoadResult.Page(
                emptyList(),
                null,
                null
            )
        }
    }

    private val viewModel = SearchViewModel(searchSourceFactory)

    @Test
    fun `should search query`() = testDispatcher.runBlockingTest {
        viewModel.changeText(query)
        viewModel.pagerFlow.first()
        verify { searchSourceFactory.createSource(query) }
    }

    @Test
    fun `should display placeholder when search is collapsed and items are gone`() =
        runBlockingTest {
            viewModel.checkPlaceholder(false, 30)
            viewModel.showPlaceHolder.asFlow().first() shouldBe false
            viewModel.checkPlaceholder(false, 0)
            viewModel.showPlaceHolder.asFlow().first() shouldBe true
        }

    @Test
    fun `should not display placeholder when search is expanded`() = runBlockingTest {
        viewModel.checkPlaceholder(true, 0)
        viewModel.showPlaceHolder.asFlow().first() shouldBe false
        viewModel.checkPlaceholder(true, 30)
        viewModel.showPlaceHolder.asFlow().first() shouldBe false
    }
}
