package com.example.skydictionary.stub

import com.example.skydictionary.search.entity.MeaningInfo
import com.example.skydictionary.search.entity.Word

fun sampleWord() = Word("1", "word", listOf(MeaningInfo("200")))
