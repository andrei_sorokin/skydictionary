package com.example.skydictionary.stub

import com.example.skydictionary.meaning.entity.Definition
import com.example.skydictionary.meaning.entity.Meaning
import com.example.skydictionary.meaning.entity.Translation

fun sampleMeaning() = Meaning(
    "1",
    "wordId",
    Definition(null, "definition"),
    emptyList(),
    emptyList(),
    "n",
    "sound",
    "text",
    "transcription",
    Translation(null, "text"),
    "updatedAt"
)
