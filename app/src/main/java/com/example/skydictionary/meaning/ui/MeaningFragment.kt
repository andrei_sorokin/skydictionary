package com.example.skydictionary.meaning.ui

import android.media.AudioAttributes
import android.media.MediaPlayer
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.skydictionary.R
import com.example.skydictionary.core.util.setupToolbar
import com.example.skydictionary.core.util.snackbar
import com.example.skydictionary.core.viewModel.peek
import com.example.skydictionary.meaning.viewModel.MeaningViewModel
import kotlinx.android.synthetic.main.appbar.toolbar
import kotlinx.android.synthetic.main.meaning_appbar.collapsingToolbar
import kotlinx.android.synthetic.main.meaning_fragment.meanings
import kotlinx.android.synthetic.main.meaning_fragment.refresh
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class MeaningFragment : Fragment(R.layout.meaning_fragment) {

    private val args: MeaningFragmentArgs by navArgs()
    private val viewModel: MeaningViewModel by viewModel { parametersOf(args.word) }

    private var mediaPlayer: MediaPlayer? = null

    private val meaningAdapter = MeaningAdapter {
        val soundUrl = "https://${it.soundUrl}"
        mediaPlayer = MediaPlayer().apply {
            setAudioAttributes(
                AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .build()
            )
            setDataSource(soundUrl)
            prepareAsync()
            setOnPreparedListener { start() }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(toolbar, collapsingToolbar)
        toolbar.title = args.word.text
        meanings.adapter = meaningAdapter
        viewModel.meanings.observe(viewLifecycleOwner) { meanings ->
            refresh.isRefreshing = false
            meaningAdapter.submitList(meanings)
        }
        viewModel.error.observe(viewLifecycleOwner) { error ->
            refresh.isRefreshing = false
            error.peek {
                snackbar(it.getMessage(resources))
                true
            }
        }
        refresh.setOnRefreshListener {
            viewModel.refresh()
        }
    }

    override fun onPause() {
        super.onPause()
        mediaPlayer?.setOnPreparedListener(null)
        mediaPlayer?.release()
        mediaPlayer = null
    }
}
