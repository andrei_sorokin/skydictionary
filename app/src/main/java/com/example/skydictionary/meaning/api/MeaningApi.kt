package com.example.skydictionary.meaning.api

import com.example.skydictionary.meaning.entity.Meaning
import retrofit2.http.GET
import retrofit2.http.Query

interface MeaningApi {

    @GET("meanings")
    suspend fun getMeanings(
        @Query("ids") ids: String,
        @Query("updatedAt") updatedAt: String? = null
    ): List<Meaning>
}
