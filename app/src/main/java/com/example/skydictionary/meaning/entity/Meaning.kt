package com.example.skydictionary.meaning.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Meaning(
    @Json(name = "id") val id: String,
    @Json(name = "wordId") val wordId: String,
    @Json(name = "definition") val definition: Definition,
    @Json(name = "examples") val examples: List<Definition>,
    @Json(name = "images") val images: List<Image>,
    @Json(name = "partOfSpeechCode") val partOfSpeechCode: String,
    @Json(name = "soundUrl") val soundUrl: String,
    @Json(name = "text") val text: String,
    @Json(name = "transcription") val transcription: String,
    @Json(name = "translation") val translation: Translation,
    @Json(name = "updatedAt") val updatedAt: String,
)
