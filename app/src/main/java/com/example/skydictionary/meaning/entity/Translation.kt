package com.example.skydictionary.meaning.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Translation(
    @Json(name = "note") val note: String?,
    @Json(name = "text") val text: String
)
