package com.example.skydictionary.meaning.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.skydictionary.R
import com.example.skydictionary.meaning.entity.Meaning
import kotlinx.android.synthetic.main.meaning_item.view.example
import kotlinx.android.synthetic.main.meaning_item.view.image
import kotlinx.android.synthetic.main.meaning_item.view.pronounceButton
import kotlinx.android.synthetic.main.meaning_item.view.title
import kotlinx.android.synthetic.main.meaning_item.view.translation

private const val MAX_EXAMPLES = 2

class MeaningViewHolder(itemView: View, private val pronounceListener: (Meaning) -> Unit) :
    RecyclerView.ViewHolder(itemView) {

    fun bind(item: Meaning) = with(itemView) {
        title.text = resources.getString(
            R.string.meaning_with_transcription,
            item.text,
            item.transcription
        )
        val imageUrl = item.images.firstOrNull()?.url?.let { "https://$it" }
        Glide.with(image)
            .load(imageUrl)
            .apply(RequestOptions.circleCropTransform())
            .into(image)
        translation.text = item.translation.text
        example.text = item.examples
            .take(MAX_EXAMPLES)
            .joinToString("\n") { it.text }
        pronounceButton.setOnClickListener { pronounceListener(item) }
    }
}
