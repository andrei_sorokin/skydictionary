package com.example.skydictionary.meaning.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.skydictionary.R
import com.example.skydictionary.meaning.entity.Meaning

class MeaningAdapter(
    diffCallback: DiffUtil.ItemCallback<Meaning> = MeaningDiffCallback,
    private val pronounceListener: (Meaning) -> Unit
) :
    ListAdapter<Meaning, MeaningViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MeaningViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.meaning_item, parent, false),
            pronounceListener
        )

    override fun onBindViewHolder(holder: MeaningViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

object MeaningDiffCallback : DiffUtil.ItemCallback<Meaning>() {
    override fun areItemsTheSame(oldItem: Meaning, newItem: Meaning) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Meaning, newItem: Meaning) =
        oldItem == newItem
}
