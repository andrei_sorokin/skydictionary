package com.example.skydictionary.meaning.di

import com.example.skydictionary.meaning.api.MeaningApi
import com.example.skydictionary.meaning.usecase.FetchMeanings
import com.example.skydictionary.meaning.viewModel.MeaningViewModel
import com.example.skydictionary.search.entity.Word
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.create

val meaningModule = module {
    single<MeaningApi> { get<Retrofit>().create() }
    single { FetchMeanings(get()) }
    viewModel { (word: Word) -> MeaningViewModel(word, get()) }
}
