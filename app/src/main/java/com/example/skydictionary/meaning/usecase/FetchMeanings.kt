package com.example.skydictionary.meaning.usecase

import com.example.skydictionary.core.util.resultOf
import com.example.skydictionary.meaning.api.MeaningApi
import com.example.skydictionary.search.entity.Word

class FetchMeanings(private val meaningApi: MeaningApi) {

    suspend fun ofWord(word: Word) = resultOf {
        val meaningsQuery = word.meanings.joinToString(",") { it.id }
        meaningApi.getMeanings(meaningsQuery)
    }
}
