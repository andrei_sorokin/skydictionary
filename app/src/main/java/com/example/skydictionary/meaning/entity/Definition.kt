package com.example.skydictionary.meaning.entity

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Definition(
    @Json(name = "soundUrl") val soundUrl: String?,
    @Json(name = "text") val text: String
)
