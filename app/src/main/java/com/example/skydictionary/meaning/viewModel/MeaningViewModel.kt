package com.example.skydictionary.meaning.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.skydictionary.core.viewModel.ErrorEvent
import com.example.skydictionary.core.viewModel.toEvent
import com.example.skydictionary.meaning.entity.Meaning
import com.example.skydictionary.meaning.usecase.FetchMeanings
import com.example.skydictionary.search.entity.Word
import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import kotlinx.coroutines.launch

class MeaningViewModel(
    private val word: Word,
    private val fetchMeanings: FetchMeanings
) : ViewModel() {

    private val _error = MutableLiveData<ErrorEvent>()
    val error: LiveData<ErrorEvent>
        get() = _error

    private val _meanings = MutableLiveData<List<Meaning>>()
    val meanings: LiveData<List<Meaning>>
        get() = _meanings

    init {
        refresh()
    }

    fun refresh() = viewModelScope.launch {
        when (val result = fetchMeanings.ofWord(word)) {
            is Ok -> _meanings.value = result.value
            is Err -> _error.value = result.error.toEvent()
        }
    }
}
