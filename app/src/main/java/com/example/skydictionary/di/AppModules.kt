package com.example.skydictionary.di

import com.example.skydictionary.meaning.di.meaningModule
import com.example.skydictionary.search.di.searchModule

val appModules = listOf(
    coreModule,
    networkModule,
    searchModule,
    meaningModule,
)
