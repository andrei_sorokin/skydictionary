package com.example.skydictionary.di

import com.squareup.moshi.Moshi
import org.koin.dsl.module

val coreModule = module {
    single {
        Moshi.Builder()
            .build()
    }
}
