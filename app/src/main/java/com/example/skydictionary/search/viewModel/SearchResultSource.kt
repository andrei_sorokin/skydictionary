package com.example.skydictionary.search.viewModel

import androidx.paging.PagingSource
import com.example.skydictionary.core.util.resultOf
import com.example.skydictionary.search.api.SearchApi
import com.example.skydictionary.search.entity.Word
import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok

class SearchResultSource(
    private val api: SearchApi,
    private val query: String
) : PagingSource<Int, Word>() {

    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, Word> {
        val nextPageNumber = params.key ?: 1
        val result = resultOf {
            if (query.isBlank()) {
                emptyList()
            } else {
                api.getWords(query, nextPageNumber, params.loadSize)
            }
        }
        return when (result) {
            is Ok -> LoadResult.Page(
                data = result.value,
                prevKey = null,
                nextKey = (nextPageNumber + 1).takeIf { result.value.isNotEmpty() }
            )
            is Err -> LoadResult.Error(result.error)
        }
    }
}
