package com.example.skydictionary.search.viewModel

import com.example.skydictionary.search.api.SearchApi

class SearchSourceFactory(
    private val api: SearchApi
) {
    fun createSource(query: CharSequence) =
        SearchResultSource(api, query.toString())
}
