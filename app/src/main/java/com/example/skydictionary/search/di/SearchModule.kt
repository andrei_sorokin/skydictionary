package com.example.skydictionary.search.di

import com.example.skydictionary.search.api.SearchApi
import com.example.skydictionary.search.viewModel.SearchSourceFactory
import com.example.skydictionary.search.viewModel.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.create

val searchModule = module {
    single<SearchApi> { get<Retrofit>().create() }
    single { SearchSourceFactory(get()) }
    viewModel { SearchViewModel(get()) }
}
