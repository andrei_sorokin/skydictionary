package com.example.skydictionary.search.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.example.skydictionary.core.util.DEBOUNCE_MS
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flatMapLatest

private const val PAGE_SIZE = 10

class SearchViewModel(
    private val searchSourceFactory: SearchSourceFactory
) : ViewModel() {

    private val searchState = MutableStateFlow("")

    private val _showPlaceHolder = MutableLiveData<Boolean>()
    val showPlaceHolder: LiveData<Boolean>
        get() = _showPlaceHolder

    val pagerFlow = searchState
        .debounce(DEBOUNCE_MS)
        .flatMapLatest { query ->
            Pager(PagingConfig(pageSize = PAGE_SIZE)) {
                searchSourceFactory.createSource(query)
            }.flow
        }.cachedIn(viewModelScope)

    fun changeText(newText: String) {
        searchState.value = newText
    }

    fun refresh(): Boolean {
        val query = searchState.value
        val shouldRefresh = query.isNotBlank()
        if (shouldRefresh) {
            changeText("")
            changeText(query)
        }
        return shouldRefresh
    }

    fun checkPlaceholder(expanded: Boolean, itemCount: Int) {
        _showPlaceHolder.value = !expanded && itemCount == 0
    }
}
