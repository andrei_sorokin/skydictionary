package com.example.skydictionary.search.entity

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class Word(
    @Json(name = "id") val id: String,
    @Json(name = "text") val text: String,
    @Json(name = "meanings") val meanings: List<MeaningInfo>,
) : Parcelable

@JsonClass(generateAdapter = true)
@Parcelize
data class MeaningInfo(
    @Json(name = "id") val id: String,
) : Parcelable
