package com.example.skydictionary.search.ui

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.addCallback
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import com.example.skydictionary.R
import com.example.skydictionary.core.util.DEBOUNCE_MS
import com.example.skydictionary.core.util.Error
import com.example.skydictionary.core.util.hideKeyboard
import com.example.skydictionary.core.util.setupToolbar
import com.example.skydictionary.core.util.snackbar
import com.example.skydictionary.core.viewModel.toEvent
import com.example.skydictionary.search.viewModel.SearchViewModel
import kotlinx.android.synthetic.main.appbar.toolbar
import kotlinx.android.synthetic.main.search_fragment.noQueryPlaceholder
import kotlinx.android.synthetic.main.search_fragment.refresh
import kotlinx.android.synthetic.main.search_fragment.searchResults
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchFragment : Fragment(R.layout.search_fragment) {

    private val viewModel: SearchViewModel by viewModel()

    private val pagingAdapter = SearchResultAdapter { word ->
        findNavController().navigate(
            SearchFragmentDirections.actionSearchFragmentToMeaningFragment(word)
        )
        hideKeyboard()
    }

    private var searchItem: MenuItem? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(toolbar)
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            if (searchItem?.isActionViewExpanded == true) {
                searchItem?.collapseActionView()
            } else {
                requireActivity().finish()
            }
        }
        toolbar.inflateMenu(R.menu.search_menu)
        searchItem = toolbar.menu.findItem(R.id.m_search)
        (searchItem?.actionView as SearchView?)?.maxWidth = Integer.MAX_VALUE
        searchResults.adapter = pagingAdapter
        refresh.setOnRefreshListener {
            refresh.isRefreshing = viewModel.refresh()
        }
        viewModel.showPlaceHolder.observe(viewLifecycleOwner) {
            noQueryPlaceholder.isVisible = it
        }
        viewLifecycleOwner.lifecycleScope.launch {
            pagingAdapter.loadStateFlow
                .debounce(DEBOUNCE_MS)
                .collectLatest { loadStates ->
                    if (loadStates.refresh !is LoadState.Loading) {
                        refresh.isRefreshing = false
                        viewModel.checkPlaceholder(
                            searchItem?.isActionViewExpanded ?: false,
                            pagingAdapter.itemCount
                        )
                    }
                    if (loadStates.refresh is LoadState.Error) {
                        val refreshError = loadStates.refresh as LoadState.Error
                        val reason = (refreshError.error as Error).toEvent().getMessage(resources)
                        snackbar(reason)
                    }
                }
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.pagerFlow
                .collectLatest {
                    pagingAdapter.submitData(it)
                }
        }
    }

    override fun onResume() {
        super.onResume()
        (searchItem?.actionView as SearchView?)?.setOnQueryTextListener(object :
                SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    viewModel.changeText(newText)
                    return true
                }
            })
        searchItem?.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                viewModel.checkPlaceholder(true, pagingAdapter.itemCount)
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                viewModel.checkPlaceholder(false, pagingAdapter.itemCount)
                return true
            }
        })
    }

    override fun onPause() {
        super.onPause()
        (searchItem?.actionView as SearchView?)?.setOnQueryTextListener(null)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        searchItem = null
    }
}
