package com.example.skydictionary.search.api

import com.example.skydictionary.search.entity.Word
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApi {

    @GET("words/search")
    suspend fun getWords(
        @Query("search") query: String,
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int
    ): List<Word>
}
