package com.example.skydictionary.search.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.skydictionary.search.entity.Word
import kotlinx.android.synthetic.main.search_result_item.view.title

class SearchResultViewHolder(itemView: View, private val clickListener: (Word) -> Unit) :
    RecyclerView.ViewHolder(itemView) {

    fun bind(item: Word?) = with(itemView) {
        item ?: return@with
        title.text = item.text
        setOnClickListener { clickListener(item) }
    }
}
