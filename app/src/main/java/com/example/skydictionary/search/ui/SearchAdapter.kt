package com.example.skydictionary.search.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.example.skydictionary.R
import com.example.skydictionary.search.entity.Word

class SearchResultAdapter(
    diffCallback: DiffUtil.ItemCallback<Word> = SearchResultDiffCallback,
    private val clickListener: (Word) -> Unit
) : PagingDataAdapter<Word, SearchResultViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SearchResultViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.search_result_item, parent, false),
            clickListener
        )

    override fun onBindViewHolder(holder: SearchResultViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

object SearchResultDiffCallback : DiffUtil.ItemCallback<Word>() {
    override fun areItemsTheSame(oldItem: Word, newItem: Word) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Word, newItem: Word) =
        oldItem == newItem
}
