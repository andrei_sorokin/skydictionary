package com.example.skydictionary

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.appbar.CollapsingToolbarLayout

class MainActivity : AppCompatActivity(R.layout.main_activity) {

    fun setupToolbar(toolbar: Toolbar, collapsingToolbarLayout: CollapsingToolbarLayout? = null) {
        val navHost = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
            as NavHostFragment
        val navController = navHost.navController
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        if (collapsingToolbarLayout != null) {
            collapsingToolbarLayout.setupWithNavController(
                toolbar,
                navController,
                appBarConfiguration
            )
        } else {
            toolbar.setupWithNavController(navController, appBarConfiguration)
        }
    }

    // TODO
//    override fun onSupportNavigateUp() =
//        navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
}
