package com.example.skydictionary.core.util

import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import retrofit2.HttpException
import java.io.IOException

@Suppress("TooGenericExceptionCaught")
suspend fun <V> resultOf(block: suspend () -> V): Result<V, Error> {
    return try {
        Ok(block())
    } catch (e: HttpException) {
        Err(Error.Http(e.code(), e.message()))
    } catch (e: IOException) {
        Err(Error.Network(e.message))
    } catch (e: Exception) {
        Err(Error.Unknown(e.message))
    }
}

inline fun <V, E> Result<V, E>.getOrNull(transform: (E) -> Unit): V? {
    return when (this) {
        is Ok -> value
        is Err -> {
            transform(error)
            null
        }
    }
}
