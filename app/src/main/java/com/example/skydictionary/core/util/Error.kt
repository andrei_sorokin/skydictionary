package com.example.skydictionary.core.util

sealed class Error(override val message: String?) : Exception(message) {

    class Network(override val message: String?) : Error(message)
    data class Http(val code: Int, override val message: String?) : Error(message)

    class Unknown(override val message: String?) : Error(message)
}
