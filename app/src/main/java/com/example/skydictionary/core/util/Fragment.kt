package com.example.skydictionary.core.util

import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.Toolbar
import androidx.core.content.getSystemService
import androidx.fragment.app.Fragment
import com.example.skydictionary.MainActivity
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.snackbar.Snackbar

fun Fragment.snackbar(message: CharSequence) {
    Snackbar.make(view!!, message, Snackbar.LENGTH_SHORT)
        .show()
}

fun Fragment.setupToolbar(toolbar: Toolbar, collapsingToolbarLayout: CollapsingToolbarLayout? = null) {
    (activity as MainActivity).setupToolbar(toolbar, collapsingToolbarLayout)
}

fun Fragment.hideKeyboard() {
    context?.getSystemService<InputMethodManager>()
        ?.hideSoftInputFromWindow(view?.windowToken, 0)
}
