package com.example.skydictionary.core.viewModel

import android.content.res.Resources
import com.example.skydictionary.R
import com.example.skydictionary.core.util.Error

@Suppress("UnnecessaryAbstractClass")
abstract class ViewEvent {

    internal var handled = false

    final override fun equals(other: Any?): Boolean {
        if (other !is ViewEvent) return false

        return other.handled == handled
    }

    final override fun hashCode(): Int = handled.hashCode()
}

/**
 * Allows observers to look at the event's type and decide if they can handle it.
 *
 * @param[handled] Lambda expression that is called with an unhandled event. Should return `true` if the event
 * was handled and `false` if it was ignored or could not be handled.
 */
fun <T : ViewEvent> T.peek(handled: (event: T) -> Boolean) {
    if (!this.handled) this.handled = handled(this)
}

data class ErrorEvent(val error: Error) : ViewEvent() {

    fun getMessage(resources: Resources) = when (error) {
        is Error.Network -> resources.getString(R.string.network_error)
        is Error.Http -> error.message ?: resources.getString(R.string.unknown_error)
        is Error.Unknown -> resources.getString(R.string.unknown_error)
    }
}

fun Error.toEvent() = ErrorEvent(this)
